package com.example.employee.repository;

import com.example.employee.repository.entity.EmployeeEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EmployeeRepository extends CrudRepository<EmployeeEntity, Long> {
    Optional<EmployeeEntity> findByEmployeeNumber(String employeeNumber);
}
