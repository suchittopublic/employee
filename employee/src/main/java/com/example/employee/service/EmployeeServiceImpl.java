package com.example.employee.service;

import com.example.employee.error.EmployeeNotFoundException;
import com.example.employee.model.Employee;
import com.example.employee.repository.EmployeeRepository;
import com.example.employee.repository.entity.EmployeeEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Override
    public Employee getEmployee(String employeeNumber) {
        Optional<EmployeeEntity> employeeEntity = employeeRepository.findByEmployeeNumber(employeeNumber);
        if (employeeEntity.isPresent()) {
            return mapEmployee(employeeEntity.get());
        }
        throw new EmployeeNotFoundException("Employee not found for employeeNumber:" + employeeNumber);
    }

    private Employee mapEmployee(EmployeeEntity employeeEntity) {
        return new Employee(employeeEntity.getEmployeeNumber(), employeeEntity.getFirstname(), employeeEntity.getLastname());
    }
}
