package com.example.employee.service;

import com.example.employee.model.Employee;

public interface EmployeeService {
    Employee getEmployee(String employeeNumber);
}
